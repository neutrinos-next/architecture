package database

import (
	"database/sql"
	"errors"
	"os"
	"time"
)

// OpenDatabase Open a connection with database
func OpenDatabase() (*sql.DB, error) {
	dbPassword := os.Getenv("SQL_PASSWORD")
	database := os.Getenv("SQL_DATABASE")
	hostname := os.Getenv("SQL_HOSTNAME")
	port := os.Getenv("SQL_PORT")

	if database == "" || dbPassword == "" || hostname == "" || port == "" {
		return nil, errors.New("Please provide valid env varibales")
	}

	connectionString := "root:" + dbPassword + "@tcp(" + hostname + ":" + port + ")/" + database

	db, err := sql.Open("mysql", connectionString)

	if err != nil {
		return nil, err
	}

	db.SetConnMaxLifetime(time.Minute * 3)
	db.SetMaxOpenConns(10)
	db.SetMaxIdleConns(10)

	return db, nil
}
