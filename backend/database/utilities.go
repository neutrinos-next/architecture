package database

import (
	"database/sql"
	"errors"
)

// URLDatabase The database struct
type URLDatabase struct {
	DB *sql.DB
}

// GetID Get ID from URL
func (db URLDatabase) GetID(url string) (int, error) {
	rows, err := db.DB.Query("SELECT id FROM urls WHERE url = ? LIMIT 1", url)

	if err != nil {
		return -1, err
	}

	defer rows.Close()

	for rows.Next() {
		var id int

		if err := rows.Scan(&id); err != nil {
			return -1, err
		}

		return id, nil
	}

	return -1, errors.New("No matching URL")
}

// AddURL Add URL to DB
func (db URLDatabase) AddURL(url string) (int, error) {

	_, err := db.DB.Exec("INSERT INTO urls (url) VALUES (?)", url)

	if err != nil {
		return -1, err
	}

	return db.GetID(url)

}

// GetURL Get the URL associated to the ID
func (db URLDatabase) GetURL(id int) (string, error) {
	rows, err := db.DB.Query("SELECT url FROM urls WHERE id = ? LIMIT 1", id)

	if err != nil {
		return "", err
	}

	defer rows.Close()

	for rows.Next() {
		var url string

		if err := rows.Scan(&url); err != nil {
			return "", err
		}

		return url, nil
	}

	return "", errors.New("URL does not exists")

}
