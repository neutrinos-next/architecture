module gitlab.com/neutrinos-next/architecture/backend

go 1.15

require (
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gofiber/fiber/v2 v2.1.2
)
