package v1

import (
	"database/sql"

	"github.com/gofiber/fiber/v2"
)

// InitV1 Initialise subgroup v1
func InitV1(app *fiber.App, db *sql.DB) {
	v1 := app.Group("/v1")

	app.Get("/healthcheck", func(c *fiber.Ctx) error {
		return c.SendStatus(fiber.StatusOK)
	})

	Shorten(v1, db)
	Get(v1, db)
}
