package v1

import (
	"database/sql"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/neutrinos-next/architecture/backend/database"
)

type request struct {
	URL string `json:"url"`
}

// Shorten Shroten an URL
func Shorten(app fiber.Router, db *sql.DB) {
	app.Post("/shorten", func(c *fiber.Ctx) error {
		body := new(request)

		if err := c.BodyParser(body); err != nil {
			return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
				"error": "Bad request",
			})
		}

		dbObj := database.URLDatabase{
			DB: db,
		}

		finalID, err := dbObj.AddURL(body.URL)

		if err != nil {
			return c.SendStatus(fiber.StatusBadRequest)
		}

		return c.Status(fiber.StatusCreated).JSON(fiber.Map{
			"id": finalID,
		})

	})
}
