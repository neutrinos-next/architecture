package v1

import (
	"database/sql"
	"strconv"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/neutrinos-next/architecture/backend/database"
)

// Get Get the URL
func Get(app fiber.Router, db *sql.DB) {
	app.Get("/url/:id", func(c *fiber.Ctx) error {
		idString := c.Params("id")

		id, err := strconv.Atoi(idString)

		if err != nil {
			return c.SendStatus(fiber.StatusBadRequest)
		}

		dbObj := database.URLDatabase{
			DB: db,
		}

		url, err := dbObj.GetURL(id)

		if err != nil {
			return c.SendStatus(fiber.StatusNotFound)
		}

		return c.Status(fiber.StatusOK).JSON(fiber.Map{
			"url": url,
		})

	})
}
