package server

import (
	"database/sql"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	v1 "gitlab.com/neutrinos-next/architecture/backend/server/v1"
)

// Init Init Go server
func Init(db *sql.DB) {
	app := fiber.New()

	app.Use(cors.New(cors.ConfigDefault))

	v1.InitV1(app, db)

	app.Listen(":9000")
}
