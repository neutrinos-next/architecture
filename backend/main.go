package main

import (
	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/neutrinos-next/architecture/backend/database"
	"gitlab.com/neutrinos-next/architecture/backend/server"
)

func main() {

	db, err := database.OpenDatabase()

	if err != nil {
		panic(err)
	}

	server.Init(db)

}
