# Get started

Please copy `envrc.dist` into `.envrc` and update variable environments

Run the SQL file located in the folder `database`

# Schemas

## Platform 2 Schema

```mermaid
flowchart TD
    C[Client]-->ALBBackend[Backend ALB]
    C --> ALBFrontend
    subgraph AWS
    subgraph Backend
    ALBBackend --> ASGBackend[Backend ASG]
    subgraph EC21Backend[Backend EC2]
    GoServer1[Go Server]
    end
    subgraph EC22Backend[Backend EC2]
    GoServer2[Go Server]
    end
    subgraph EC23Backend[Backend EC2]
    GoServer3[Go Server]
    end
    ASGBackend --> GoServer1
    ASGBackend --> GoServer2
    ASGBackend --> GoServer3
    end
    subgraph Database
    GoServer1 --> MariaDB
    GoServer2 --> MariaDB
    GoServer3 --> MariaDB
    MariaDB[MariaDB Database]
    MariaDB2[MariaDB Database 2]
    MariaDB <--> MariaDB2
    end

    subgraph Frontend
    ALBFrontend --> ASGFrontend[Frontend ASG]
    subgraph EC21Frontend[Frontend EC2]
    CaddyServer1[Caddy Server]
    StaticFiles1[Static files]
    CaddyServer1 --> StaticFiles1
    end
    subgraph EC22Frontend[Frontend EC2]
    CaddyServer2[Caddy Server]
    StaticFiles2[Static files]
    CaddyServer2 --> StaticFiles2
    end
    subgraph EC23Frontend[Frontend EC2]
    CaddyServer3[Caddy Server]
    StaticFiles3[Static files]
    CaddyServer3 --> StaticFiles3
    end
    ASGFrontend --> CaddyServer1
    ASGFrontend --> CaddyServer2
    ASGFrontend --> CaddyServer3
    end
    end
```

## Platform 3 Schema

```mermaid
flowchart TB
  C[Client]-->S3[Public S3 Bucket]
  C-->BS
  BS-->G1
  BS-->G2
  BS-->G3
  subgraph AWS
  subgraph Frontend
  S3
  end
  subgraph Backend
  subgraph K8[Kubernetes]
  BS[Backend Service]
  subgraph HPA[Horizontal Pod Autoscaler]
  subgraph Pod1
  G1[GoServer]
  end
  subgraph Pod2
  G2[GoServer]
  end
  subgraph Pod3
  G3[GoServer]
  end
  end
  end
  end
  G1 --> RDS
  G2 --> RDS
  G3 --> RDS
  subgraph DB[Database]
  RDS[Amazon Relational Database Service]
  end
  end

```

Groupe 6

-   Thanh Tâm-Tanguy Tran
-   Alexandre Delaunay
-   Killian Siou
-   Robin Boucher
-   Hugo Fouquet
-   Erwan Bazille
