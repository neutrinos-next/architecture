import axios from "axios"

const API_URL = process.env.REACT_APP_API_URL

export class ApiHelper {
    public async shorten(url: string): Promise<{ id: number }> {
        const data = await axios.post(`${API_URL}/v1/shorten`, {
            url,
        })

        return data.data
    }

    public async getRealUrl(id: string): Promise<{ url: string }> {
        const data = await axios.get(`${API_URL}/v1/url/${id}`)

        return data.data
    }
}
