import React, { useEffect, useState } from "react"

import { ApiHelper } from "api/api"

import { Link, useParams } from "react-router-dom"

const Short = () => {
    const { id } = useParams<{ id: string }>()

    const [redirectUrl, setRedirectUrl] = useState<string>()
    const [isNotFound, setIsNotFound] = useState(false)

    useEffect(() => {
        new ApiHelper()
            .getRealUrl(id)
            .then(data => {
                setIsNotFound(false)
                setRedirectUrl(data.url)
            })
            .catch(() => {
                setIsNotFound(true)
            })
    }, [id])

    if (isNotFound) {
        return (
            <div className="flex flex-col gap-2 mt-16 al items-center">
                <div className="text-4xl text-red-700 font-bold">URL not found</div>
                <Link
                    to="/"
                    className="bg-blue-200 text-blue-800 font-bold rounded py-2 px-4"
                >
                    <button>Back to home page</button>
                </Link>
            </div>
        )
    }

    if (!redirectUrl) {
        return <div className="mt-16 text-4xl text-blue-900 font-bold">Loading...</div>
    }

    window.location.href = redirectUrl

    return <div className="mt-16 text-4xl text-blue-900 font-bold">Loading...</div>
}

export default Short
