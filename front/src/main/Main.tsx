import React, { useEffect, useState } from "react"

import { ApiHelper } from "api/api"
import copy from "copy-to-clipboard"
import { FiClipboard, FiX } from "react-icons/fi"
import { useToasts } from "react-toast-notifications"

interface ShortUrl {
    realUrl: string
    shorten: string
}

const LOCAL_STORAGE_KEY = "__shortent_list__"

const getHostname = () => {
    return window.location.protocol + "//" + window.location.host + "/#"
}

const Main = () => {
    const [urlInput, setUrl] = useState("")
    const [urls, setUrls] = useState<Array<ShortUrl>>(
        JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY) || "[]") || [],
    )

    const { addToast } = useToasts()

    const handleSubmit = async () => {
        console.log("TOOT")
        const idRes = await new ApiHelper().shorten(urlInput)

        const addToList: ShortUrl = {
            realUrl: urlInput,
            shorten: `${getHostname()}/short/${idRes.id}`,
        }

        copy(addToList.shorten)
        addToast("URL copied into clipboard", {
            appearance: "success",
            autoDismiss: true,
        })

        setUrls([addToList, ...urls])

        setUrl("")
    }

    useEffect(() => {
        localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(urls))
    }, [urls])

    return (
        <div className="container">
            <div className="h-full flex items-strech gap-16 flex-col pt-32">
                <div className="text-center">
                    <h1 className="font-bold text-4xl text-blue-700">URL Shortener</h1>
                </div>
                <form
                    className="flex items-strech gap-4"
                    onSubmit={e => {
                        e.preventDefault()
                        handleSubmit()
                    }}
                >
                    <input
                        className="rounded shadow-sm border border-blue-100 bg-white flex px-6 py-4 outline-none flex-grow"
                        placeholder="URL to shorten"
                        value={urlInput}
                        onChange={e => setUrl(e.target.value)}
                    />
                    <button
                        type="submit"
                        className="px-8 rounded shadow-sm bg-blue-700 font-bold text-white border border-blue-800 outline-none"
                    >
                        Shorten
                    </button>
                </form>
                {urls.length !== 0 && (
                    <div className="bg-white border border-blue-100 rounded shadow-sm overflow-none">
                        {urls.map(url => (
                            <div
                                key={url.shorten}
                                className="flex items-center overflow-none px-4 py-2 hover:bg-gray-100 gap-4 flex-shrink-0"
                            >
                                <FiClipboard
                                    className="text-gray-600 cursor-pointer h-full"
                                    onClick={() => {
                                        copy(url.shorten)
                                        addToast("URL copied into clipboard", {
                                            appearance: "success",
                                            autoDismiss: true,
                                        })
                                    }}
                                />
                                <div className="flex-grow">{url.shorten}</div>
                                <div className="text-gray-500 text-xs">{url.realUrl}</div>
                                <FiX
                                    className="flex-shrink-0 text-gray-600 cursor-pointer hover:text-gray-800"
                                    onClick={() => {
                                        const newArray = urls.filter(
                                            urlElm => url.shorten !== urlElm.shorten,
                                        )

                                        setUrls(newArray)
                                    }}
                                />
                            </div>
                        ))}
                    </div>
                )}
            </div>
        </div>
    )
}

export default Main
