import React from "react"
import "./App.css"
import "./styles/main.css"
import { HashRouter, Route } from "react-router-dom"
import { ToastProvider } from "react-toast-notifications"

import Main from "./main/Main"
import Short from "./short/Short"

function App() {
    return (
        <ToastProvider>
            <div className="min-h-screen bg-blue-100 flex justify-center items-strech">
                <HashRouter>
                    <Route exact path="/">
                        <Main />
                    </Route>
                    <Route path="/short/:id">
                        <Short />
                    </Route>
                </HashRouter>
            </div>
        </ToastProvider>
    )
}

export default App
