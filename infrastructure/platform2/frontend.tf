resource "aws_launch_configuration" "frontend_launch_conf" {
  name            = "frontend_launch_conf"
  image_id        = data.aws_ami.default_ami.id
  instance_type   = "t2.small"
  spot_price      = "0.01"
  security_groups = [aws_security_group.asg_security_group_frontend.id]
  key_name        = var.user_key

  lifecycle {
    create_before_destroy = true
  }

  user_data = <<EOF
#!/bin/bash
apt update
export HOME=/root
apt-get remove yarn || true
wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.37.0/install.sh | bash
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
nvm install node
nvm use node
npm install -g yarn
git clone https://gitlab.com/neutrinos-next/architecture.git
cd architecture/front
export REACT_APP_API_URL=http://${aws_lb.backend_alb.dns_name}:9000
yarn install
yarn run build
echo "deb [trusted=yes] https://apt.fury.io/caddy/ /" \
    | tee -a /etc/apt/sources.list.d/caddy-fury.list
apt update
apt install caddy -y
systemctl stop caddy
caddy file-server --root ./build --listen "0.0.0.0:80"
  EOF

}


resource "aws_autoscaling_group" "frontend_asg" {
  name                 = "frontend_asg"
  launch_configuration = aws_launch_configuration.frontend_launch_conf.name
  vpc_zone_identifier  = concat(aws_subnet.frontend_subnet.*.id)

  min_size                  = 3
  max_size                  = 3
  desired_capacity          = 3
  health_check_grace_period = 30
  health_check_type         = "ELB"

  target_group_arns = [aws_lb_target_group.asg_target_group_frontend.arn]

  tag {
    key                 = "Name"
    value               = "Frontend ASG"
    propagate_at_launch = true
  }

  tag {
    key                 = "Kind"
    value               = "frontend"
    propagate_at_launch = true
  }

  lifecycle {
    create_before_destroy = true
  }
}



resource "aws_lb" "frontend_alb" {
  name            = "FrontendALB"
  internal        = false
  security_groups = [aws_security_group.alb_security_group_frontend.id]
  subnets         = aws_subnet.frontend_subnet.*.id

  depends_on = [
    aws_internet_gateway.frontend_gateway
  ]

}

resource "aws_security_group" "asg_security_group_frontend" {
  name        = "ASG frontend security group"
  description = "Security group of the frontend ASG"

  vpc_id = aws_vpc.frontend.id

  ingress {
    description     = "Allow web"
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    security_groups = [aws_security_group.alb_security_group_frontend.id]
  }

  ingress {
    description = "Allow SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Security Frontend ASG"
    Kind = "frontend"
  }
}

resource "aws_security_group" "alb_security_group_frontend" {
  name        = "ALB frontend security group"
  description = "Allow http to ALB"


  vpc_id = aws_vpc.frontend.id

  ingress {
    description = "Allow web"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    description = "Allow web"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Security Frontent ALB"
    Kind = "frontend"
  }
}

resource "aws_lb_target_group" "asg_target_group_frontend" {
  name = "ASGTargetgroupFrontend"

  port = 80

  protocol = "HTTP"
  vpc_id   = aws_vpc.frontend.id

  health_check {
    enabled             = true
    path                = "/"
    port                = 80
    matcher             = "200-299"
    healthy_threshold   = 2
    unhealthy_threshold = 10
    interval            = 60
  }

}

resource "aws_lb_listener" "frontend_listener" {
  load_balancer_arn = aws_lb.frontend_alb.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.asg_target_group_frontend.arn
  }
}
