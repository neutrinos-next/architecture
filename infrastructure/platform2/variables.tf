variable "region" {
  default = "eu-west-3"
}


variable "SQL_PASSWORD" {
  type = string
}

variable "SQL_DATABASE" {
  type = string
}

variable "SQL_PORT" {
  type    = number
  default = 3306
}

variable "user_key" {
  type    = string
  default = null
}
