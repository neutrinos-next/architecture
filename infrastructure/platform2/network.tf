
data "aws_availability_zones" "available" {
  state = "available"
}

# BACKEND

resource "aws_vpc" "backend" {
  cidr_block = "10.0.0.0/16"

  tags = {
    Name = "VPC Backend"
    Kind = "backend"
  }
}

resource "aws_subnet" "backend_subnet" {

  count = 4

  vpc_id                  = aws_vpc.backend.id
  cidr_block              = ["10.0.0.0/24", "10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"][count.index]
  map_public_ip_on_launch = true
  availability_zone       = data.aws_availability_zones.available.names[count.index % length(data.aws_availability_zones.available.names)]

  tags = {
    Name = "Subnet Backend ${count.index}"
    Kind = "backend"
  }
}

resource "aws_route_table" "backend_route_table" {
  vpc_id = aws_vpc.backend.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.backend_gateway.id
  }

  route {
    ipv6_cidr_block = "::/0"
    gateway_id      = aws_internet_gateway.backend_gateway.id
  }

}


resource "aws_internet_gateway" "backend_gateway" {
  vpc_id = aws_vpc.backend.id

  tags = {
    Name = "main"
    Kind = "backend"
  }
}


# resource "aws_main_route_table_association" "route_table_association" {
#   vpc_id         = aws_vpc.backend.id
#   route_table_id = aws_route_table.backend_route_table.id
# }

resource "aws_route_table_association" "backend_associtation" {

  count = 4

  subnet_id      = aws_subnet.backend_subnet[count.index].id
  route_table_id = aws_route_table.backend_route_table.id
}

# FRONTEND

resource "aws_vpc" "frontend" {
  cidr_block = "10.0.0.0/16"

  tags = {
    Name = "VPC Frontend"
    Kind = "frontend"
  }
}

resource "aws_subnet" "frontend_subnet" {

  count = 2

  vpc_id                  = aws_vpc.frontend.id
  cidr_block              = ["10.0.0.0/24", "10.0.1.0/24"][count.index]
  map_public_ip_on_launch = true
  availability_zone       = data.aws_availability_zones.available.names[count.index]

  tags = {
    Name = "Subnet Frontend ${count.index}"
    Kind = "frontend"
  }
}

resource "aws_route_table" "frontend_route_table" {
  vpc_id = aws_vpc.frontend.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.frontend_gateway.id
  }

  route {
    ipv6_cidr_block = "::/0"
    gateway_id      = aws_internet_gateway.frontend_gateway.id
  }

}

resource "aws_internet_gateway" "frontend_gateway" {
  vpc_id = aws_vpc.frontend.id

  tags = {
    Name = "main"
    Kind = "frontend"
  }
}

resource "aws_main_route_table_association" "route_table_association_frontend" {
  vpc_id         = aws_vpc.frontend.id
  route_table_id = aws_route_table.frontend_route_table.id
}
