resource "aws_security_group" "database_security_group" {
  name        = "Database security group"
  description = "Allow conection to database"

  vpc_id = aws_vpc.backend.id

  ingress {
    description     = "Allow conection to database"
    from_port       = var.SQL_PORT
    to_port         = var.SQL_PORT
    protocol        = "tcp"
    security_groups = [aws_security_group.asg_security_group.id]
  }

  ingress {
    description = "Allow SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Allow to communicate between DB"
    from_port   = var.SQL_PORT
    to_port     = var.SQL_PORT
    protocol    = "tcp"
    self        = true
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Security Group Database"
    Kind = "database"
  }
}

locals {
  private_db_ip = ["10.0.0.127", "10.0.0.87"]
}

resource "aws_instance" "database" {

  count = 2

  ami           = data.aws_ami.default_ami.id
  instance_type = "t2.small"
  subnet_id     = aws_subnet.backend_subnet[0].id
  key_name      = var.user_key

  private_ip = local.private_db_ip[count.index]

  user_data = <<EOF
#!/bin/bash
apt-get update
export HOME=/root
apt-get remove docker docker-engine docker.io containerd runc -y
apt-get update
apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common \
    mariadb-server -y

git clone https://gitlab.com/neutrinos-next/architecture.git
cd architecture
export SQL_PASSWORD=${var.SQL_PASSWORD}
export SQL_DATABASE=${var.SQL_DATABASE}
apt-get install mysql-client -y
cd
systemctl stop mariadb || true
systemctl set-environment MYSQLD_OPTS="--skip-grant-tables --skip-networking"
systemctl start mariadb

sleep 5

mysql --batch -e "SELECT PASSWORD('${var.SQL_PASSWORD}')" > password-db.txt
export HASH_PASSWORD_DB=$(tail -n 1 password-db.txt)

echo "FLUSH PRIVILEGES;
UPDATE mysql.user SET password = PASSWORD('${var.SQL_PASSWORD}') WHERE user = 'root';
UPDATE mysql.user SET authentication_string = '' WHERE user = 'root';
UPDATE mysql.user SET plugin = '' WHERE user = 'root';
grant all privileges on *.* to 'root'@'%' identified by password '$HASH_PASSWORD_DB' with grant option;" > edit-root.sql

echo "--> Editing ROOT"
mysql < edit-root.sql
echo "--< Editing ROOT"
sed -i 's/^bind-address.*$//' /etc/mysql/mariadb.conf.d/50-server.cnf
systemctl unset-environment MYSQLD_OPTS
systemctl restart mariadb
sleep 10

echo "[mysql]
user=root
password=${var.SQL_PASSWORD}" > .my.cnf
chmod 600 .my.cnf

echo "CREATE DATABASE \`${var.SQL_DATABASE}\`;" > setup-db.sql
mysql < setup-db.sql

mysql ${var.SQL_DATABASE} < /architecture/database/database-01.sql


echo "server_id           = ${count.index + 1}
log_bin             = /var/log/mysql/mysql-bin.log
log_bin_index       = /var/log/mysql/mysql-bin.log.index
relay_log           = /var/log/mysql/mysql-relay-bin
relay_log_index     = /var/log/mysql/mysql-relay-bin.index
max_binlog_size     = 100M
log_slave_updates   = 1
auto-increment-increment = 2
auto-increment-offset = 1
bind-address            = ${local.private_db_ip[count.index]}" >> /etc/mysql/mariadb.conf.d/50-server.cnf

systemctl restart mariadb
sleep 5

echo "GRANT ALL PRIVILEGES ON *.* TO 'replication'@'${local.private_db_ip[1 - count.index]}' IDENTIFIED BY '${var.SQL_PASSWORD}';
FLUSH PRIVILEGES;" > replication.sql

mysql < ./replication.sql

sleep 30

mariadb -ureplication -h ${local.private_db_ip[1 - count.index]} --batch -e "SHOW MASTER STATUS;" | sed -E -n "s/.*${"\t"}([0-9]+)${"\t"}.*/\1/p" > position.txt
export DB_POSITION=$(cat position.txt)

echo "STOP SLAVE;
CHANGE MASTER TO master_host='${local.private_db_ip[1 - count.index]}', master_port=3306, master_user='replication', master_password='${var.SQL_PASSWORD}', master_log_file='mysql-bin.000001', master_log_pos=$DB_POSITION;
START SLAVE;" > replication-step2.sql

mariadb < replication-step2.sql

  EOF

  tags = {
    Name = "Database"
    Kind = "database"
  }

  security_groups             = [aws_security_group.database_security_group.id]
  associate_public_ip_address = true

  root_block_device {
    volume_size           = 30
    delete_on_termination = true
  }

}
