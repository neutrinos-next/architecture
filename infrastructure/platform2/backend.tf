
resource "aws_launch_configuration" "backend_launch_conf" {
  name            = "backand_launch_conf"
  image_id        = data.aws_ami.default_ami.id
  instance_type   = "t2.small"
  spot_price      = "0.01"
  security_groups = [aws_security_group.asg_security_group.id]
  key_name        = var.user_key

  lifecycle {
    create_before_destroy = true
  }

  user_data = <<EOF
#!/bin/bash
apt update

echo "cd /
add-apt-repository ppa:longsleep/golang-backports -y
apt update
apt install golang-go -y
git clone https://gitlab.com/neutrinos-next/architecture.git
cd architecture/backend
export HOME=/root
export SQL_PASSWORD=${var.SQL_PASSWORD}
export SQL_DATABASE=${var.SQL_DATABASE}
export SQL_HOSTNAME=${aws_instance.database[0].private_ip}
export SQL_PORT=${var.SQL_PORT}
go build -o backend
./backend" > init.sh
chmod u+x init.sh
./init.sh
  EOF

}

resource "aws_autoscaling_group" "backend_asg" {
  name                 = "backend_asg"
  launch_configuration = aws_launch_configuration.backend_launch_conf.name
  vpc_zone_identifier  = [aws_subnet.backend_subnet[2].id, aws_subnet.backend_subnet[3].id]

  min_size                  = 3
  max_size                  = 3
  desired_capacity          = 3
  health_check_grace_period = 30
  health_check_type         = "ELB"

  target_group_arns = [aws_lb_target_group.asg_target_group.arn]

  tag {
    key                 = "Name"
    value               = "Backend ASG"
    propagate_at_launch = true
  }

  tag {
    key                 = "Kind"
    value               = "backend"
    propagate_at_launch = true
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_lb" "backend_alb" {
  name            = "BackendALB"
  internal        = false
  security_groups = [aws_security_group.alb_security_group.id]
  subnets         = [aws_subnet.backend_subnet[2].id, aws_subnet.backend_subnet[3].id]

  depends_on = [
    aws_internet_gateway.backend_gateway
  ]

}

resource "aws_security_group" "asg_security_group" {
  name        = "ASG backend security group"
  description = "Allow http to ASG"

  vpc_id = aws_vpc.backend.id

  ingress {
    description     = "Allow web"
    from_port       = 9000
    to_port         = 9000
    protocol        = "tcp"
    security_groups = [aws_security_group.alb_security_group.id]
  }

  ingress {
    description = "Allow SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }


  tags = {
    Name = "Security Backend ASG"
    Kind = "backend"
  }

}

resource "aws_security_group" "alb_security_group" {
  name        = "ALB backend security group"
  description = "Allow http to ALB"


  vpc_id = aws_vpc.backend.id

  ingress {
    description = "Allow web"
    from_port   = 9000
    to_port     = 9000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 9000
    to_port     = 9000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }


  tags = {
    Name = "Security Backend ALB"
    Kind = "backend"
  }
}

resource "aws_lb_target_group" "asg_target_group" {
  name = "ASGTargetgroupBackend"

  port = 9000

  protocol = "HTTP"
  vpc_id   = aws_vpc.backend.id

  health_check {
    enabled             = true
    path                = "/healthcheck"
    port                = 9000
    matcher             = "200-299"
    healthy_threshold   = 2
    unhealthy_threshold = 10
    interval            = 60
  }

}

resource "aws_lb_listener" "backend_listener" {
  load_balancer_arn = aws_lb.backend_alb.arn
  port              = "9000"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.asg_target_group.arn
  }
}


