resource "aws_iam_user" "cli_user" {
  name = "cli-user"

  path = "/system/"

  tags = {
    Name = "CLI User Terraform"
  }
}

resource "aws_iam_access_key" "cli_key" {
  user = aws_iam_user.cli_user.name
}

resource "aws_iam_user_policy" "cli_policy" {
  name = "cli-policy"
  user = aws_iam_user.cli_user.name

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
        {
            "Sid": "AllObjectActions",
            "Effect": "Allow",
            "Action": "s3:*",
            "Resource": "*"
        }
    ]
}
EOF
}

locals {
  aws_cli = {
    key_id     = aws_iam_access_key.cli_key.id
    access_key = aws_iam_access_key.cli_key.secret
  }
}
