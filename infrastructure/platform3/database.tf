resource "aws_security_group" "database_security_group" {

  name        = "Database Security Group"
  description = "Allow EKS to connect to database"

  vpc_id = module.vpc.vpc_id

  egress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description     = "Allow from EKS"
    from_port       = 3306
    to_port         = 3306
    protocol        = "TCP"
    security_groups = [aws_security_group.eks_security_group.id]
  }

}

module "database" {
  source  = "terraform-aws-modules/rds/aws"
  version = "~> 2.20"

  identifier = "databaseshortener"

  engine            = "mariadb"
  engine_version    = "10.4"
  instance_class    = "db.t3.small"
  allocated_storage = 5

  name     = var.SQL_DATABASE
  username = "root"
  password = var.SQL_PASSWORD
  port     = "3306"

  vpc_security_group_ids = [aws_security_group.database_security_group.id]

  maintenance_window = "Mon:00:00-Mon:03:00"
  backup_window      = "03:00-06:00"

  multi_az = true

  # disable backups to create DB faster
  backup_retention_period = 0

  tags = {
    Name = "database"
  }

  # DB subnet group
  subnet_ids = module.vpc.private_subnets

  # DB parameter group
  family = "mariadb10.4"

  # DB option group
  major_engine_version = "10.4"

  # Snapshot name upon DB deletion
  final_snapshot_identifier = "database"

  # Database Deletion Protection
  deletion_protection = false

  parameters = [
    {
      name  = "character_set_client"
      value = "utf8"
    },
    {
      name  = "character_set_server"
      value = "utf8"
    }
  ]

}
