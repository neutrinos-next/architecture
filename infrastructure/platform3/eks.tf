locals {
  cluster_name = "shortener-cluster"
}

resource "aws_security_group" "eks_security_group" {
  name   = "EKS Security group"
  vpc_id = module.vpc.vpc_id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Allow backend connection"
    from_port   = 9000
    to_port     = 9000
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

data "aws_eks_cluster" "cluster" {
  name = module.eks-cluster.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks-cluster.cluster_id
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority.0.data)
  token                  = data.aws_eks_cluster_auth.cluster.token
  load_config_file       = false
  version                = "~> 1.9"
}

module "eks-cluster" {
  source          = "terraform-aws-modules/eks/aws"
  cluster_name    = local.cluster_name
  cluster_version = "1.18"
  subnets         = module.vpc.private_subnets
  vpc_id          = module.vpc.vpc_id

  tags = {
    Name = local.cluster_name
  }

  # fargate_profiles = {
  #   default_profile = {
  #     namespace = "default"
  #   }
  # }

  worker_groups_launch_template = [{
    name                    = "spot-backend"
    override_instance_types = ["t3a.small", "t3.small", "t2.small"]
    spot_instance_pools     = 3
    asg_max_size            = 10
    asg_desired_capacity    = 3
    kubelet_extra_args      = "--node-labels=node.kubernetes.io/lifecycle=spot"
    public_ip               = false
  }]

  worker_additional_security_group_ids = [aws_security_group.eks_security_group.id]
  cluster_security_group_id            = aws_security_group.eks_security_group.id
}
