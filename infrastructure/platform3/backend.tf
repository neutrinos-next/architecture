
locals {

  db_password = var.SQL_PASSWORD
  db_db       = var.SQL_DATABASE

}

locals {
  database_ip = module.database.this_db_instance_address
}

resource "kubernetes_job" "init_database" {

  metadata {
    name = "database-init-job"
  }

  spec {
    template {
      metadata {}

      spec {
        container {
          image = "ubuntu:18.04"
          name  = "db-job"
          command = ["bash", "-c", <<EOF
apt-get update -y
export HOME=/root
apt-get install mysql-client git -y
sleep 20
cd /
git clone https://gitlab.com/neutrinos-next/architecture.git
cd
echo "[mysql]
user=root
password='${local.db_password}'" > .my.cnf
chmod 600 .my.cnf
mysql -h ${local.database_ip} --port 3306 ${local.db_db} < /architecture/database/database-01.sql
EOF
          ]
        }

        restart_policy = "Never"

      }
    }

    backoff_limit = 0
  }

  wait_for_completion = true

  timeouts {
    create = "5m"
  }

}

locals {
  backend_labels = {
    name = "backend"
  }
}

resource "kubernetes_deployment" "backend" {
  metadata {
    name = "backend"

    labels = local.backend_labels

  }

  spec {

    replicas = 3

    selector {
      match_labels = local.backend_labels
    }

    template {
      metadata {
        labels = local.backend_labels
      }

      spec {

        container {

          image   = "golang:alpine3.12"
          name    = "backend"
          command = ["sh", "-c", "apk add git && git clone https://gitlab.com/neutrinos-next/architecture.git && cd architecture/backend && export HOME=/root && go build -o backend && ./backend"]

          resources {
            limits {
              cpu    = "0.5"
              memory = "512Mi"
            }
          }

          port {
            container_port = 9000
          }

          env {
            name  = "SQL_PASSWORD"
            value = local.db_password
          }

          env {
            name  = "SQL_DATABASE"
            value = local.db_db
          }

          env {
            name  = "SQL_HOSTNAME"
            value = local.database_ip
          }

          env {
            name  = "SQL_PORT"
            value = 3306
          }


          liveness_probe {
            http_get {
              path = "/healthcheck"
              port = 9000
            }

            initial_delay_seconds = 60
            period_seconds        = 5
            timeout_seconds       = 5

          }

        }

      }

    }

  }
}

resource "kubernetes_service" "backend" {
  metadata {
    name = "backend-service"
  }

  spec {
    selector = {
      name = kubernetes_deployment.backend.metadata[0].labels.name
    }

    port {
      port        = 9000
      target_port = 9000
    }

    type = "LoadBalancer"
  }
}


resource "kubernetes_horizontal_pod_autoscaler" "backend_autoscale" {

  metadata {
    name = "backend-autoscaler"
  }

  spec {
    min_replicas = 3
    max_replicas = 15

    scale_target_ref {
      api_version = "apps/v1"
      kind        = "Deployment"
      name        = kubernetes_deployment.backend.metadata[0].name
    }

    target_cpu_utilization_percentage = 50
  }
}

resource "kubernetes_pod" "bastion" {

  metadata {
    name = "bastion"
  }

  spec {
    container {
      image   = "ubuntu:21.04"
      name    = "bastion-img"
      command = ["sh", "-c", "tail -f /dev/null"]
    }
  }
}

resource "helm_release" "metrics_server" {
  name       = "metrics-server"
  repository = "https://charts.bitnami.com/bitnami"
  chart      = "metrics-server"

  set {
    name  = "apiService.create"
    value = "true"
  }
}

locals {
  backend_url = kubernetes_service.backend.load_balancer_ingress[0].hostname
}
