output "backend_url" {
  value = local.backend_url
}

output "frontend_url" {
  value = aws_s3_bucket.front_files.website_endpoint
}
