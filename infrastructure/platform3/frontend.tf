resource "aws_s3_bucket" "front_files" {
  bucket = "front-files-server"

  acl = "public-read"

  force_destroy = true

  tags = {
    Name = "Front bucket"
  }

  website {
    index_document = "index.html"
  }
}

resource "kubernetes_job" "build_front" {
  metadata {
    name = "build-front-job"
  }

  spec {
    template {
      metadata {}

      spec {
        container {
          image = "node:15-alpine3.10"
          name  = "build-front"

          command = ["sh", "-c", <<EOF
apk add git
apk add --no-cache python3 py3-pip && pip3 install --upgrade pip && pip3 install awscli
cd /
git clone https://gitlab.com/neutrinos-next/architecture.git
cd architecture/front
yarn install
yarn run build
aws s3 sync build s3://${aws_s3_bucket.front_files.bucket}/ --acl public-read
EOF
          ]

          env {
            name  = "AWS_ACCESS_KEY_ID"
            value = local.aws_cli.key_id
          }

          env {
            name  = "AWS_SECRET_ACCESS_KEY"
            value = local.aws_cli.access_key
          }

          env {
            name  = "AWS_DEFAULT_REGION"
            value = var.region
          }

          env {
            name  = "REACT_APP_API_URL"
            value = "http://${local.backend_url}:9000"
          }
        }

        restart_policy = "Never"

      }
    }

    backoff_limit = 0
  }

  wait_for_completion = true

  timeouts {
    create = "10m"
  }

}
